#!/usr/bin/env python3

# Copyright 2021 Xavier Claessens <xclaesse@gmail.com>

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import io
import os
import sys
import configparser
import shutil
import hashlib
import requests
import tempfile
import typing as T
import subprocess
import json

from pathlib import Path
from utils import is_ci, is_debianlike

api_url = os.environ.get("CI_API_V4_URL", "https://gitlab.com/api/v4")

class CreateRelease:
    def __init__(self, repo: T.Optional[str], token: T.Optional[str], tag: str):
        print('Preparing release for:', tag)
        self.tag = tag
        self.name, self.version = self.tag.split('/', 1)
        self.repo = repo
        self.token = token

        with tempfile.TemporaryDirectory() as self.tempdir:
            self.read_wrap()
            self.create_patch_zip()
            self.create_source_fallback()
            self.create_wrap_file()

    def read_wrap(self):
        filename = Path('subprojects', self.name + '.wrap')
        if not filename.exists():
            raise ValueError(f"Wrapfile {filename} not present")
        self.wrap = configparser.ConfigParser(interpolation=None)
        self.wrap.read(filename)
        self.wrap_section = self.wrap[self.wrap.sections()[0]]

    def create_patch_zip(self):
        patch_directory = self.wrap_section.get('patch_directory')
        if patch_directory is None:
            return

        directory = self.wrap_section.get('directory', self.name)
        srcdir = Path('subprojects', 'packagefiles', patch_directory)
        destdir = Path(self.tempdir, directory)

        generator = Path(srcdir, 'generator.sh')
        if generator.exists():
            try:
                fn = 'ci_config.json'
                with open(fn, 'r') as f:
                    ci = json.load(f)
            except json.decoder.JSONDecodeError:
                raise RuntimeError(f'file {fn} is malformed')

            debian_packages = ci.get(self.name, {}).get('debian_packages', [])
            if debian_packages and is_debianlike():
                if is_ci():
                    subprocess.check_call(['sudo', 'apt-get', '-y', 'install'] + debian_packages)
                else:
                    s = ', '.join(debian_packages)
                    print(f'The following packages could be required: {s}')

            subprocess.check_call([generator])

        # If no specific license is specified, copy wrapdb's
        license_file = srcdir / 'LICENSE.build'
        remove_license = False
        if not license_file.exists():
            shutil.copyfile('COPYING', license_file)
            remove_license = True

        shutil.copytree(srcdir, destdir)
        base_name = Path(self.tempdir, f'{self.name}-{self.version}_patch')
        shutil.make_archive(base_name.as_posix(), 'zip', root_dir=self.tempdir, base_dir=directory)

        if remove_license:
            license_file.unlink()

        patch_filename = base_name.with_name(base_name.name + '.zip')
        self.upload(patch_filename, 'application/zip')

        h = hashlib.sha256()
        h.update(patch_filename.read_bytes())
        patch_hash = h.hexdigest()

        del self.wrap_section['patch_directory']
        self.wrap_section['patch_filename'] = patch_filename.name
        self.wrap_section['patch_url'] = f'{api_url}/projects/{self.repo}/packages/generic/{self.name}/{self.version}/{patch_filename.name}'
        self.wrap_section['patch_hash'] = patch_hash

    def create_wrap_file(self):
        self.wrap_section['wrapdb_version'] = self.version

        filename = Path(self.tempdir, self.name + '.wrap')

        # configparser write() adds multiple trailing newlines, collapse them
        buf = io.StringIO()
        self.wrap.write(buf)
        buf.seek(0)
        newbuf = buf.read().rstrip('\n') + '\n'

        with open(filename, 'w') as f:
            f.write(newbuf)

        print('Generated wrap file:')
        print(filename.read_text())
        self.upload(filename, 'text/plain')

    def upload(self, path: Path, mimetype: str):
        if not self.repo or not self.token:
            # Write files locally when not run on CI
            Path('subprojects', 'packagecache').mkdir(parents=True, exist_ok=True)
            with Path('subprojects', 'packagecache', path.name).open('wb') as f:
                f.write(path.read_bytes())
            return
        headers = {
            'JOB-TOKEN': self.token,
        }
        response = requests.put(f'{api_url}/projects/{self.repo}/packages/generic/{self.name}/{self.version}/{path.name}', headers=headers, data=path.read_bytes())
        response.raise_for_status()

    def create_source_fallback(self):
        response = requests.get(self.wrap_section['source_url'])
        response.raise_for_status()
        filename = Path(self.tempdir, self.wrap_section['source_filename'])
        filename.write_bytes(response.content)
        self.upload(filename, 'application/zip')
        self.wrap_section['source_fallback_url'] = f'{api_url}/projects/{self.repo}/packages/generic/{self.name}/{self.version}/{filename.name}'

if __name__ == '__main__':
    # Support local testing when passing no arguments
    repo = sys.argv[2] if len(sys.argv) > 2 else None
    token = sys.argv[3] if len(sys.argv) > 3 else None
    CreateRelease(repo, token, sys.argv[1])
